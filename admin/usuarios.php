<!DOCTYPE html>
<?php
require_once '../lib/Usuario.php';

require_once '../lib/funciones.php';

require 'headerAdmin.php';
 ?>
 <?php
 if (isset($_GET['status']) && $_GET['status']==1){
   ?>
   <div class="alert alert-success" role="alert">
     <button type="button" class="close" data-dismiss="alert" arial-label="Close">
       <span aria-hidden="true">&times;</span>
     </button>
     <strong>Creado!</strong>El usuario ha sido creado correctamente
   </div>
   <script type="text/javascript">
   window.setTimeout(function() {
      /*$(".alert").fadeTo(500, 0).slideUp(500, function(){
          $(this).remove();
      });*/
      $(".alert").fadeTo(500, 0);
    }, 4000);
   </script>
   <?php
 }

  ?>

    <div class="container">
      <a href="formUser.php" class="btn btn-success" role="button">Nuevo Usuario</a>
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Email</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $usuarios = Usuario::getAllUsers();
            foreach ($usuarios as $usuario) {
              ?>
              <tr>
                <td><?php echo $usuario->id; ?></td>
                <td><?php echo $usuario->nombre; ?></td>
                <td><?php echo $usuario->apellidos; ?></td>
                <td><?php echo $usuario->email; ?></td>
                <td>
                  <a href="formUser.php?id=<?php echo $usuario->id; ?>" class="btn btn-primary" role="button"><i class="fas fa-user-edit"></i></a>
                </td>

                <td> <a href="formUser.php?action=delete&id=<?php echo $usuario->id; ?>" class="btn btn-danger" role="button"><i class="fas fa-trash-alt"></i></a> </td>
              </tr>
              <?php
            }
           ?>

        </tbody>
      </table>

    </div>
<?php
require 'footerAdmin.php';
 ?>
