<?php
require_once ('headerAdmin.php');
require_once ('../lib/funciones.php');
require_once ('../lib/Usuario.php');
 ?>
 <div class="" id="LoginForm">
   <div class="container">
     <h1 class="form-heading">login Form</h1>
     <div class="login-form">
       <div class="main-div">
         <div class="panel">
           <h2>Login</h2>
           <p>Introduce usuario y contraseña</p>
         </div>
         <form id="Login" action="" method="post">
           <div class="form-group">
             <input type="text" class="form-control" name="inputLogin" placeholder="Login">
           </div>
           <div class="form-group">
             <input type="password" class="form-control" name="inputPassword" placeholder="Password">
           </div>
           <div class="forgot">
             <a href="reset.php">Recuperar contraseña</a>
           </div>
           <input type="submit" class="btn btn-primary" value="Iniciar sesión">
         </form>
       </div>
     </div>
   </div>
 </div>
 </div>

<?php

if (!empty($_POST)){
  Usuario::validateLogin($_POST['inputLogin'],$_POST['inputPassword']);
}

require_once ('footerAdmin.php');
?>
