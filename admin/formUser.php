<?php
require 'headerAdmin.php';
require_once '../lib/funciones.php';
require_once '../lib/Usuario.php';

if (isset($_GET['action'])){
  if ($_GET['action']=="delete") {
    Usuario::deleteById($_GET['id']);
    header("Location: usuarios.php");

  }
}


$id=0;
$usuario = new Usuario();
$disabled = "";

if (isset($_GET['id'])){
  $id = $_GET['id'];
  $usuario = Usuario::getById($id);
  $disabled = " disabled ";
}
 ?>
<div class="container">
  <form class="row" action="" method="post">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
      <label for="login">Login</label>
      <input type="text" <?php echo $disabled; ?> name="login" value="<?php echo $usuario->login; ?>" class="form-control" placeholder="Introduce el login">
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
      <label for="nombre">Nombre</label>
      <input type="text" name="nombre" value="<?php echo $usuario->nombre; ?>" class="form-control" placeholder="Introduce el nombre">
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
      <label for="apellidos">Apellidos</label>
      <input type="text" name="apellidos" value="<?php echo $usuario->apellidos; ?>" class="form-control">
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
      <label for="email">Email</label>
      <input type="email" name="email" value="<?php echo $usuario->email; ?>" class="form-control">
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
      <label for="pass1">Contraseña</label>
      <input type="password" name="pass1" value="" class="form-control">
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
      <label for="pass2">Repite Contraseña</label>
      <input type="password" name="pass2" value="" class="form-control">
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-primary">Guardar Usuario</button>

    </div>
  </form>

</div>
<?php
if (!empty($_POST)){
  if ($_POST['pass1']==$_POST['pass2']){
    $id = null;
    if ($_POST['id']!=0){
      $id=$_POST['id'];
    }
    $usuario = new Usuario(
      $_POST['login'],
      $_POST['nombre'],
      $_POST['apellidos'],
      $_POST['email'],
      $_POST['pass1'],
      $id
    );
    $usuario->save();
    header("Location: usuarios.php?status=1");
  }else{
    echo "Las contraseñas no son iguales";
  }

}
?>



<?php
require 'footerAdmin.php';
 ?>
