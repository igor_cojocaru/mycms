<?php

class Usuario {
  public $nombre;
  public $id;
  public $email;
  public $apellidos;
  public $login;
  public $password;
  public $last_login;

  function __construct ($login="", $nombre="", $apellidos="", $email="", $password="", $id = null) {
    $this->login = $login;
    $this->nombre = $nombre;
    $this->apellidos = $apellidos;
    $this->email = $email;
    $this->password = $password;
    $this->id = $id;
  }

  function save(){
    //me conecto con la base de datos
    $conn = db_connect();

    if ($this->id==null){
      $stmt = $conn->prepare("INSERT INTO usuarios (nombre, apellidos, login, email, password) VALUES (?,?,?,?,?)");
      $stmt->bind_param("sssss",$this->nombre, $this->apellidos, $this->login, $this->email, $this->password);
      $stmt->execute();
      $this->id = $conn->insert_id;
    }else {
      $stmt = $conn->prepare("UPDATE usuarios SET nombre=?, apellidos=?, email=?, password=? WHERE id=?");
      $stmt->bind_param("ssssi",$this->nombre, $this->apellidos, $this->email, $this->password, $this->id);
      $stmt->execute();
    }
    //cierro
    $stmt->close();
    $conn->close();
  }

  static function getById($value) {
    //conecto con la base de datos
    $conn = db_connect();

    //preparo la consulta. Obtener los datos del usuario con el id que me pasan por parámetro
    $sql = "SELECT id, nombre, apellidos, login, email, password FROM usuarios Where id=".$value;

    //ejecuto la consulta y almaceno los resultados
    $result = $conn->query($sql);

    //si el resultado contiene filas
    $usuario = null;
    if ($result->num_rows > 0) {
    // mostramos la información de cada fila
        while ($fila = $result->fetch_assoc()){
          //creo un objeto con los datos
          $usuario = new Usuario(
            $fila['login'],
            $fila['nombre'],
            $fila['apellidos'],
            $fila['email'],
            $fila['password'],
            $fila['id']
          );
        }
    }
    $conn->close();
    return $usuario;
  }

  static function getAllUsers() {
    $conn = db_connect();

    $sql = "SELECT id, nombre, apellidos, login, email, password FROM usuarios";
    $result = $conn->query($sql);

    //creo un array de resultados
    $arrayResults = [];

    if ($result->num_rows > 0) {
        while ($fila = $result->fetch_assoc()){
            //creo un objeto Usuario y lo añado al array
            $arrayResults[] = new Usuario($fila['login'], $fila['nombre'], $fila['apellidos'], $fila['email'], $fila['password'], $fila['id']);
        }
    }

    $conn->close();
    return $arrayResults;

  }

  static function deleteById($value) {
    $conn = db_connect();

    $stmt = $conn->prepare("DELETE FROM usuarios WHERE id=?");
    $stmt->bind_param("i", $value);
    $stmt->execute();

    $stmt->close();
    $conn->close();
  }

  static function validateLogin($login, $pass) {
    $conn = db_connect();

    $query = "select password from usuarios where login ='".$login."'";

    $result = $conn->query($query);

    if ($result->num_rows==1) {
      $fila = $result->fetch_assoc();
      $userPass = $fila['password'];
      if ($pass==$userPass){
        echo "contraseña correcta";
      }else{
        echo "datos incorrectos";
      }
    }else{
      echo "datos incorrectos";
    }
  }


  /**
     * Get the value of Nombre
     *
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of Nombre
     *
     * @param mixed nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Email
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param mixed email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of Apellidos
     *
     * @return mixed
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set the value of Apellidos
     *
     * @param mixed apellidos
     *
     * @return self
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get the value of Login
     *
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set the value of Login
     *
     * @param mixed login
     *
     * @return self
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get the value of Password
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of Password
     *
     * @param mixed password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of Last Login
     *
     * @return mixed
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * Set the value of Last Login
     *
     * @param mixed last_login
     *
     * @return self
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;

        return $this;
    }

}

 ?>
