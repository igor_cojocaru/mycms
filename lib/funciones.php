<?php
//defino constantes
define ('DB_HOST','localhost');
define ('DB_USER','root');
define ('DB_PASS','');
define ('DB_NAME','mycms');

function db_connect() {
  $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  if ($conn->connect_errno) {
    echo "Error: no se ha podido conectar ".$conn->connect_errno." ".$conn->connect_error;
    die();
  }
  return $conn;
}


?>
